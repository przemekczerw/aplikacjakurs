package czerw.przemek.aplikacjakurs;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;

/**
 * Created by przemyslawczerw on 26.07.2017.
 */

public class MojReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        long[] pattern = {0, 500, 300};
        NaszeMetody.ShowMessage("Wkonalo sie");
        Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
//        v.vibrate([500,110,500,110,450,110,200,110,170,40,450,110,200,110,170,40,500]);
        //  v.vibrate(500);
        v.vibrate(300);

    }
}
