package czerw.przemek.aplikacjakurs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.ButterKnife;

public class PanstwaActivity extends AppCompatActivity {

    //@BindView(R.id.listView) ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panstwa);
        ButterKnife.bind(this);

        String[] panstwa = {"Polska", "Francja", "Włochy", "Szkocja", "Szwecja", "Rosja", "Czechy", "Usa", "Australia", "Belgia", "Irlandia", "Portugalia", "Hiszpania", "Anglia", "Brazylia", "Argentyna"};

        ArrayList<String> panstwaL = new ArrayList<String>();
        panstwaL.addAll(Arrays.asList(panstwa));
        // ustawiamy domyslny wyglad listy
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, panstwaL);

        // ustawaimy wlasny adapter
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.row, panstwaL);

//        listView.setAdapter(adapter);
//
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                // odnosze sie do kontrolki Textview w moim R.layout.row
//                TextView selectedTextView = ((TextView) view.findViewById(R.id.mojTextView));
//                selectedTextView.setTextColor(getResources().getColor(R.color.colorAccent));
//
//                // pobieram wartosc tekstowa kliknietego elemenut za pomoca getItemAtPosition
//                String tekst = (String) parent.getItemAtPosition(position);
//
//                Toast.makeText(getApplicationContext(), tekst, Toast.LENGTH_SHORT).show();
//
//
//            }
//        });


    }
}
