package czerw.przemek.aplikacjakurs;

import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.renderscript.Sampler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AkcelerometrActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor mAccelerometr, mTemperature;
    @BindView(R.id.OsX) TextView OsX;
    @BindView(R.id.OsY) TextView OsY;
    @BindView(R.id.OsZ) TextView OsZ;
    @BindView(R.id.activity_akcelerometr) LinearLayout activity_akcelerometr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_akcelerometr);
        ButterKnife.bind(this);

         mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
         mAccelerometr = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mTemperature = mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometr, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mTemperature, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
    Sensor mySensor = sensorEvent.sensor;
        float ambient_temperature = sensorEvent.values[0];

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER)  {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];
            OsX.setText(String.valueOf(x));
            OsY.setText(String.valueOf(y));
            OsZ.setText(String.valueOf(z));
            if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER){

                activity_akcelerometr.setBackgroundColor(Color.parseColor("#ffff" + String.valueOf(ambient_temperature)));
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
