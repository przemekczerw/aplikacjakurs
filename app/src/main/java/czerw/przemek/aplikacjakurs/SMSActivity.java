package czerw.przemek.aplikacjakurs;

/**
 * Created by przemyslawczerw on 13.07.2017.
 */

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SMSActivity extends AppCompatActivity {

    EditText numerTelefonu;
    EditText tekstWiadomosci;
    Button btnSMS;
    Button btnObliczeniowy;

    // stala dzieki ktorej po odpowiedzi od aplikacji o statusie przyznania dostepu bedziemy
    // wiedziec ze jest to odpowiedz na dane nasze wywolanei
    final int MY_PERMISSIONS_REQUEST_WRITE_SMS = 1;
    final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 2;

    // nasz TAG za pomoca ktorego bedziemy mogli filtrowac wiadomosci w Android Monitor
    public static final String TAG = "PrzemekApp";

    CoordinatorLayout layoutSms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms);
        numerTelefonu = (EditText) findViewById(R.id.numerTelefonu);
        tekstWiadomosci = (EditText) findViewById(R.id.textWiadomosci);

        layoutSms = (CoordinatorLayout) findViewById(R.id.layout_sms);


        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSMSClick();
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarSms);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Manager");

        Intent i = getIntent();
        String suma = i.getStringExtra("suma");
        tekstWiadomosci.setText("Na zakupy musze miec: " + suma + "zł");


    }

    // aby utworzyc menu z pliku xml
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sms, menu);
        return true;
    }

    // aby przypisac akcje do elementow w menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.idCall:
                onPhoneClick();
                return true;
            case R.id.idSensSms:
                onSMSClick();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void wykonajTelefon() {
        String telString = "tel:" + numerTelefonu.getText().toString();
        Intent i = new Intent(Intent.ACTION_CALL);
        i.setData(Uri.parse(telString));
        startActivity(i);
    }

    // wlasna funkcja ktora przyjmuje numer telefonu i tresc wiadomosci
    private void sendSms(String phoneNo, String msg) {
        try {
            //wykorzystujemy smsmanager czyli wbudowane api do zarzadzania smsami
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, msg, null, null);

            // za pomoca Log mozemy zrobic log momencie wyslania SMS . Ten log jest widoczny w ANdroid Monitorze.
            Log.d(TAG, "SMS Wysłany");

            // wyslanie snackabara
//            Snackbar mySnackbar = Snackbar.make(layoutSms, "Sms Wysłany", Snackbar.LENGTH_SHORT);
//            mySnackbar.show();
            NaszeMetody.PrintLog("SMS WYSLANY");
//            NaszeMetody.ShowMessage("NO WITAM");
            NaszeMetody.ShowSnackBar(layoutSms, "SMS WYSLANY");

            // ponizej na dwa sposoby czyscimy wpisane kontrolki
            numerTelefonu.setText("");
            tekstWiadomosci.getText().clear();


            // ustaiamy result i zamykamy
            setResult(RESULT_OK);
            finish();

        } catch (Exception ex) {
            Log.d(TAG, "SMS Nie wysłany");
            ex.printStackTrace();
        }
    }


    // wlasna funkcja ktora przyjmuje numer telefonu i tresc wiadomosci
    public void onSMSClick() {

        // sprawdzamy czy jest przyznay dostep
        if (android.support.v4.app.ActivityCompat.checkSelfPermission(SMSActivity.this, Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {

            // za pomoca tej funkcji sprawdzamy czy uzytkownik po raz pierwszy juz blokowal dostep do sms
            if (ActivityCompat.shouldShowRequestPermissionRationale(SMSActivity.this,
                    Manifest.permission.SEND_SMS)) {
                // jesli dostep blokowal pokazujemy po co nam to potrzebne
                showExplanation("Potrzebujemy pozwolenia", "Chcemy wysłać SMS który napisałeś, więc potrzebujemy pozwolenia",
                        Manifest.permission.SEND_SMS, MY_PERMISSIONS_REQUEST_WRITE_SMS);
            } else {
                // pokazujemy okienko z prosba za pierwszym razem odrazu systemowe
                requestPermissions(Manifest.permission.SEND_SMS, MY_PERMISSIONS_REQUEST_WRITE_SMS);
            }
        } else {
            // z Edittext za pomoca funkcji getText().toString() wykonanej na kontrolce pobieramy wpisany tekst
            // sendsms to wlasna funkcja do wysylania smsow opisana ponizej
            sendSms(numerTelefonu.getText().toString(), tekstWiadomosci.getText().toString());

        }
    }

    // funkcja ktora pokazuje okienko systemowe z prosba o dany kod
    private void requestPermissions(String permissionName, int permissionRequestCode) {
        ActivityCompat.requestPermissions(this, new String[]{permissionName}, permissionRequestCode);
    }

    // wlasna funkcja ktora pokazuje okienko z wyjasnieniem prosby o dostep
    private void showExplanation(String title, String message, final String permission, final int permissionRequestCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                requestPermissions(permission, permissionRequestCode);
            }
        });
        builder.show();
    }


    // metoda wywolywana za kazdym razem gdy uzytkownik podejmie decyzje o dostepie
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_SMS: {
                // jesli uzytkownik dal anuluj to dlugosc listy bedzie pusta
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendSms(numerTelefonu.getText().toString(), tekstWiadomosci.getText().toString());
                    // dostep przyznany - mozemy zrobic co chcemy
                    Log.d(TAG, "Dostęp przyznany");
                } else {
                    Log.d(TAG, "Dostęp nie przyznany");
                    //  dostep nie przyznany ! musimy obsluzyc ten problem w aplikacji
                    // ponizej dodatkowo sprawdzamy czy zaznaczyl never ask again
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        // user rejected the permission
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(SMSActivity.this,
                                Manifest.permission.SEND_SMS);
                        if (!showRationale) {
                            Log.d(TAG, "Uzytkownik zaznaczyl never ask again");
                        }
                    }
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_CALL_PHONE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    wykonajTelefon();
                    // dostep przyznany - mozemy zrobic co chcemy
                    Log.d(TAG, "Dostęp przyznany");
                } else {
                    Snackbar mySnackbar = Snackbar.make(layoutSms, "Dostęp nie przyznany", Snackbar.LENGTH_SHORT);
                    mySnackbar.show();
                    //  dostep nie przyznany ! musimy obsluzyc ten problem w aplikacji
                    // ponizej dodatkowo sprawdzamy czy zaznaczyl never ask again
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        // user rejected the permission
                        boolean showRationale = ActivityCompat.shouldShowRequestPermissionRationale(SMSActivity.this,
                                Manifest.permission.CALL_PHONE);
                        if (!showRationale) {
                            Log.d(TAG, "Uzytkownik zaznaczyl never ask again");
                        }
                    }
                }
                return;
            }

            // za pomoca swticha mozna przejrzec czasmi wiele prosb
        }
    }

    public void onPhoneClick() {

        // sprawdzamy czy jest przyznay dostep
        if (android.support.v4.app.ActivityCompat.checkSelfPermission(SMSActivity.this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            // za pomoca tej funkcji sprawdzamy czy uzytkownik po raz pierwszy juz blokowal dostep do sms
            if (ActivityCompat.shouldShowRequestPermissionRationale(SMSActivity.this,
                    Manifest.permission.CALL_PHONE)) {
                // jesli dostep blokowal pokazujemy po co nam to potrzebne
                showExplanation("Potrzebujemy pozwolenia", "Chcemy zadzwonić do osoby, której numer podałeś, więc potrzebujemy pozwolenia",
                        Manifest.permission.CALL_PHONE, MY_PERMISSIONS_REQUEST_CALL_PHONE);
            } else {
                // pokazujemy okienko z prosba za pierwszym razem odrazu systemowe
                requestPermissions(Manifest.permission.CALL_PHONE, MY_PERMISSIONS_REQUEST_CALL_PHONE);
            }
        } else {
            // z Edittext za pomoca funkcji getText().toString() wykonanej na kontrolce pobieramy wpisany tekst
            // sendsms to wlasna funkcja do wysylania smsow opisana ponizej
            wykonajTelefon();
        }
    }


}