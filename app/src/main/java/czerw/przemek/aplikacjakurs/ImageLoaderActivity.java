package czerw.przemek.aplikacjakurs;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImageLoaderActivity extends AppCompatActivity {


    @BindView(R.id.photo) ImageView viewPhoto;
    @BindView(R.id.loadPic) Button loadPic;
    @BindView(R.id.activity_image_loader) RelativeLayout activity_image_loader;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_image_loader);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_image_activity);

        setSupportActionBar(toolbar);
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
        .build();
        ImageLoader.getInstance().init(config);
        ButterKnife.bind(this);

            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public void showSnackBar() {
        Snackbar snackbarAction = Snackbar.make(activity_image_loader, "Możesz się cofnać tutaj ->  ", Snackbar.LENGTH_LONG)
                .setAction("Cofnij", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(ImageLoaderActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                });

        snackbarAction.show();

    }

    public void getClassID() {

    }

//    @OnClick(R.id.loadPic)
//    public void showPicinImageView() {
//
//        String url = "https://amazingslider.com/wp-content/uploads/2012/12/dandelion.jpg";
//        ImageLoader.getInstance().displayImage(url, viewPhoto);
//    }

    @OnClick(R.id.viewImageButton)
    public void loadPic() {
        showSnackBar();
        String url = "https://amazingslider.com/wp-content/uploads/2012/12/dandelion.jpg";
        ImageLoader.getInstance().displayImage(url, viewPhoto);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
     if(item.getItemId() == R.id.play_music_toolbar_icon)
         finish();
        return super.onOptionsItemSelected(item);
    }
}
