package czerw.przemek.aplikacjakurs;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

/**
 * Created by przemyslawczerw on 14.06.2017.
 */

public class PanstwaAdapter extends RecyclerView.Adapter<PanstwaAdapter.MyViewHolder> {
    private List<Panstwa> panstwoLista;
    ImageLoader imageLoader;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.wiersz_listy_panstwa, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Panstwa obiektPanstwa = panstwoLista.get(position);
        holder.pan.setText(obiektPanstwa.nazwa_panstwa);
        holder.st.setText(obiektPanstwa.stolica_panstwa);
        imageLoader.displayImage("drawable://R.drawable.flaga", holder.zdjFl);
    }

    @Override
    public int getItemCount() {
        return panstwoLista.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView st;
        public TextView pan;
        public ImageView zdjFl;

        public MyViewHolder(View itemView) {
            super(itemView);
            st = (TextView) itemView.findViewById(R.id.stolicaID);
            pan = (TextView) itemView.findViewById(R.id.panstwoID);
            zdjFl = (ImageView) itemView.findViewById(R.id.flagaID);

        }
    }

    public PanstwaAdapter(List<Panstwa> panstwoLista) {
        this.panstwoLista = panstwoLista;
        imageLoader.getInstance();

    }
}
