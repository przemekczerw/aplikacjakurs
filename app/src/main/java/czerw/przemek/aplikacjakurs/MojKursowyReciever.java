package czerw.przemek.aplikacjakurs;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by przemyslawczerw on 08.06.2017.
 */

public class MojKursowyReciever extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {

            if(intent.getAction().equals("android.intent.action.ACTION_POWER_CONNECTED"))
                Toast.makeText(context, "Telefon został podłączony do ładowarki", Toast.LENGTH_LONG).show();

            if(intent.getAction().equals("android.intent.action.ACTION_POWER_DISCONNECTED"))
                Toast.makeText(context, "Telefon został odłączony od ładowarki", Toast.LENGTH_LONG).show();
        }
    }

