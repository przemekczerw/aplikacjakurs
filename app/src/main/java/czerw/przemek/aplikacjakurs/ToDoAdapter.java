package czerw.przemek.aplikacjakurs;

/**
 * Created by przemyslawczerw on 14.07.2017.
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import czerw.przemek.aplikacjakurs.Dane.ToDoItem;
import czerw.przemek.aplikacjakurs.Dane.ZapytaniaBazy;


public class ToDoAdapter extends RecyclerView.Adapter<ToDoAdapter.MyViewHolder> {

    private List<ToDoItem> listaZadan;
    ZapytaniaBazy polaczenieBazy;
    private Context context;

    public ToDoAdapter(List<ToDoItem> zadaniaLista, Context context) {
        this.listaZadan = zadaniaLista;
        this.context = context;
        polaczenieBazy = new ZapytaniaBazy(context);
        polaczenieBazy.otworzZapis();
    }

    @Override
    public ToDoAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.wiersz_listy_todo, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ToDoAdapter.MyViewHolder holder, final int position) {

        final ToDoItem item = listaZadan.get(position);


        holder.trescZadania.setText(item.getNazwaZadania());

        holder.edytujZadanie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(v.getContext());
                alertDialog.setTitle("Wpisz zadanie do wykonania");

                final EditText nowyText = new EditText(v.getContext());
                nowyText.setText(item.getNazwaZadania());
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                nowyText.setLayoutParams(lp);
                alertDialog.setView(nowyText);

                alertDialog.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        polaczenieBazy.edytujWierszToDo(nowyText.getText().toString(), String.valueOf(item.getId()));
                        listaZadan = polaczenieBazy.zwrocListeToDo();
                        notifyDataSetChanged();

                    }
                });

                alertDialog.show();

            }
        });
        holder.usunZadanie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                polaczenieBazy.usunWierszToDo(String.valueOf(item.getId()));
                listaZadan = polaczenieBazy.zwrocListeToDo();
                notifyDataSetChanged();
            }
        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                    Intent intent = new Intent(context, SzczegolyToDoListActivity.class);
                    intent.putExtra("wiadomsoc", item.getSzczegoly());
                    context.startActivity(intent);

                } else if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {

                }
                NaszeMetody.ShowMessage("Klika sie");
            }
        });
    }


    @Override
    public int getItemCount() {
        return listaZadan.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView trescZadania;
        public ImageButton edytujZadanie;
        public ImageButton usunZadanie;


        public MyViewHolder(View itemView) {
            super(itemView);
            trescZadania = (TextView) itemView.findViewById(R.id.txtNazwaZadania);
            edytujZadanie = (ImageButton) itemView.findViewById(R.id.ibEdytuj);
            usunZadanie = (ImageButton) itemView.findViewById(R.id.ibUsun);
        }


    }
}
