package czerw.przemek.aplikacjakurs;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;

public class SecondActivity extends AppCompatActivity {

    String settedText;
    @BindView(R.id.myUrlInWebView) WebView webView;
    @BindView(R.id.tvOK) TextView tvOK;

    @OnClick(R.id.buttonOnSecondActivity)
    public void goToWeb() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getAddresWithHttps(settedText)));
        startActivity(intent);
    }
    @OnLongClick(R.id.buttonOnSecondActivity)
    public boolean goToWebLongClick() {
        webView.loadUrl(getAddresWithHttps(settedText));
        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent intent = getIntent();
        settedText = intent.getStringExtra("dana_kod");
       // tvOK.setText(settedText);
        Button buttonOnSecondActivity = (Button) findViewById(R.id.buttonOnSecondActivity);
//
//        buttonOnSecondActivity.setOnClickListener(onWebClick);
//
//        buttonOnSecondActivity.setOnLongClickListener(newLongWebClick);



    }

//
//    public View.OnClickListener onWebClick = new View.OnClickListener(){
//        public void onClick(View v) {
//            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getAddresWithHttps(settedText)));
//            startActivity(intent);
//        }
//    };

//    public View.OnLongClickListener newLongWebClick = new View.OnLongClickListener(){
//        public boolean onLongClick(View v){
//            webView.setWebViewClient(new WebViewClient());
//            webView.loadUrl(getAddresWithHttps(settedText));
//            return true;
//        }
//    };

    public String getAddresWithHttps(String address) {
        return address.toLowerCase().startsWith("http://") ? address.toLowerCase() : "http://" + address.toLowerCase();
    }
}






