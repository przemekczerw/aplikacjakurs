package czerw.przemek.aplikacjakurs;

import java.io.Serializable;

/**
 * Created by przemyslawczerw on 06.06.2017.
 */

public class Uzytkownik implements Serializable {
    public String imie, nazwisko, email, kodPocztowy, numerTelefonu, panstwo;
    public boolean isCheckSwitch;

    public Uzytkownik(String imie, String nazwisko, String email, String kodPocztowy,
                      String numerTelefonu, String panstwo, boolean isCheckSwitch) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.email = email;
        this.kodPocztowy = kodPocztowy;
        this.numerTelefonu = numerTelefonu;
        this.panstwo = panstwo;
        this.isCheckSwitch = isCheckSwitch;
    }
//
//    public String isChecked() {
//        if (isCheckSwitch) {
//            return "Uzytkownik jest zainteresowany kursem";
//        } else return "Uzytkownik nie jest zainteresowany kursem";
//    }
}

