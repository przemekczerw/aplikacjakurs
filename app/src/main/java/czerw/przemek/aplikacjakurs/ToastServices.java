package czerw.przemek.aplikacjakurs;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by przemyslawczerw on 08.06.2017.
 */


public class ToastServices extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    Toast toast;
    Timer timer;
    TimerTask timerTask;
    int counter;

    private class MojTimerTast extends TimerTask{

        @Override
        public void run() {
            counter++;
            toast.setText("Wisisz mi: " + counter + "$");
            toast.show();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        toast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        timer = new Timer();
        timerTask = new MojTimerTast();
        timer.scheduleAtFixedRate(timerTask, 1000, 5*1000);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.purge();
        timerTask.cancel();
    }
}
