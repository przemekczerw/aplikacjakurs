package czerw.przemek.aplikacjakurs;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.IOException;

public class TakeAPhotoActivity extends AppCompatActivity {

    private static final int MAKE_IMAGE_REQUEST = 1;
    private static final int PICK_IMAGE_REQUEST = 2;
    String photoPath = "";
    private ImageView showPicture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_aphoto);

        if (savedInstanceState != null) {
            photoPath = savedInstanceState.getString(IMAGE);
            Glide.with(getApplicationContext()).load(photoPath).into(showPicture);
        }

        this.showPicture = (ImageView) findViewById(R.id.showPictureFromCamera);

        ImageButton zrobZdjecieAparatem = (ImageButton) findViewById(R.id.take_a_picture);
        zrobZdjecieAparatem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, MAKE_IMAGE_REQUEST);
            }
        });

        ImageButton idzDoGalerii = (ImageButton) findViewById(R.id.go_to_gallery);
        idzDoGalerii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
// Show only images, no videos or anything else
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
// Always show the chooser (if there are multiple options available)
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Uri uri = null;
        if (requestCode == MAKE_IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
//            Bitmap photo = (Bitmap) data.getExtras().get("data");
//            showPicture.setImageBitmap(photo);

            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    new String[]{MediaStore.Images.Media.DATA, MediaStore.Images.Media.DATE_ADDED,
                            MediaStore.Images.ImageColumns.ORIENTATION}, MediaStore.Images.Media.DATE_ADDED, null, "date_added ASC");
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    uri = Uri.parse(cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA)));
                    photoPath = uri.toString();
                } while (cursor.moveToNext());
                cursor.close();
            }

            Glide.with(getApplicationContext()).load("file://" + photoPath).into(showPicture);

        } else if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                // Log.d(TAG, String.valueOf(bitmap));

                showPicture.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    static final String IMAGE = "image";

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(IMAGE, photoPath);
        super.onSaveInstanceState(outState);
    }


}



