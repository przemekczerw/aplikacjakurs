package czerw.przemek.aplikacjakurs;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ZasobyActivity extends AppCompatActivity {
    @BindView(R.id.Change) Button ChangeButton;
    @BindView(R.id.Play) Button PlayButton;
    @BindView(R.id.imageView) ImageView picture;
    @BindView(R.id.Blink) Button BlinkButton;
    MediaPlayer mp;
    boolean isPlayingMusic = false;
    boolean isZoomed = false;
    Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zasoby);
        ButterKnife.bind(this);

    }
    @OnClick(R.id.Play)
    public void musicPlays() {
        if(!isPlayingMusic){
            mp = MediaPlayer.create(this, R.raw.tetris_soundtrack);
            mp.start();
            isPlayingMusic = true;
        } else {
            mp.stop();
            isPlayingMusic = false;
        }
    }
    @OnClick(R.id.Zoom)
    public void zoomPic(){
        if(isZoomed) {
            picture.setScaleX(1);
            picture.setScaleY(1);
            isZoomed = false;
        } else {
            picture.setScaleX(1.5f);
            picture.setScaleY(1.5f);
            isZoomed = true;
        }
    }
    @OnClick(R.id.Blink)
    public void blinkingPicutre() {
        animation = new AlphaAnimation(1, 0);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        picture.startAnimation(animation);
    }
}
