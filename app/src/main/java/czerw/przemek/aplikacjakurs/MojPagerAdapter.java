package czerw.przemek.aplikacjakurs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by przemyslawczerw on 14.06.2017.
 */

public class MojPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;


    public MojPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new BlankFragment();
            case 1:
                return new ZdjecieFragment();
            case 2:
                return new ListaMuzykiFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}