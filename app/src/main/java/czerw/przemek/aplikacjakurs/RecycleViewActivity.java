package czerw.przemek.aplikacjakurs;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecycleViewActivity extends AppCompatActivity {
    @BindView(R.id.recycle_view)
    RecyclerView recycle_view;
    Panstwa panstwa;

    ImageLoader imageLoader;

    //    RecyclerView mojRecyclerView = (RecyclerView) findViewById(R.id.recycle_view);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycle_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        List<Panstwa> listaPanstwa = new ArrayList<>();
        Panstwa poland = new Panstwa("Polska", "Warszawa", R.drawable.poland);
        Panstwa unitedKingdom = new Panstwa("Wielka Brytania", "Londyn", R.drawable.united_kingdom);
        Panstwa portugal = new Panstwa("Portugalia", "Lizbona", R.drawable.portugal);

        listaPanstwa.add(poland);
        listaPanstwa.add(unitedKingdom);
        listaPanstwa.add(portugal);

        ButterKnife.bind(this);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        recycle_view.setLayoutManager(llm);

        PanstwaAdapter pa = new PanstwaAdapter(listaPanstwa);
        recycle_view.setAdapter(pa);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);


    }

}
