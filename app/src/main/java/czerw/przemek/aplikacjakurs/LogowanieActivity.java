package czerw.przemek.aplikacjakurs;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LogowanieActivity extends AppCompatActivity {

    @BindView(R.id.etPassword)
    EditText etPass;

    @BindView(R.id.etLogin)
    EditText etLogin;

    @BindView(R.id.etPasswordWrapper)
    TextInputLayout etPassWrapper;

    @BindView(R.id.etLoginWrapper)
    TextInputLayout etLoginWrapper;

    @OnClick(R.id.btnZaloguj)
    public void logowanie(){

        String email = etLogin.getText().toString();
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            etLoginWrapper.setError("Wpisz poprawny email");
            return;
        }else{
            etLoginWrapper.setError(null);
        }
        if(etPass.getText().length()< 10){
            etPassWrapper.setError("Za krótkie hasło");
            return;
        }else{
            etPassWrapper.setError(null);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logowanie);
        ButterKnife.bind(this);


            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            String login = sharedPreferences.getString(getString(R.string.PREF_LOGIN), "");
            String haslo = sharedPreferences.getString(getString(R.string.PREF_HASLO), "");

            etLogin.setText(login);
            etPass.setText(haslo);


    }

    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
