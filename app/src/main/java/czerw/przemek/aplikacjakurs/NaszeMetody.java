package czerw.przemek.aplikacjakurs;

/**
 * Created by przemyslawczerw on 13.07.2017.
 */

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class NaszeMetody {

    public static final String LOG_TAG = "appppp";
    public static Context AppContext;

    public static void InicjalizacjaNaszeMetody(Context appContext) {
        AppContext = appContext;
    }

    public static void PrintLog(String s) {
        Log.d(LOG_TAG, s);
    }

    public static void ShowMessage(String msg) {
        Toast.makeText(AppContext, msg, Toast.LENGTH_SHORT).show();
    }

    public static void ShowSnackBar(View view, String msg) {
        Snackbar mySnackbar = Snackbar.make(view, msg, Snackbar.LENGTH_SHORT);
        mySnackbar.show();
    }

    public static void ThreadSleep(Integer integer) {
        try {
            Thread.sleep(integer);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static String returnImageURI(int link) {
        // aby otworzyc z resource bitmapy za pomoca ImageLoadera korzystam z wlasnej funkcji
        // ktora do nazwy zasobow dodaje przedrostek drawable://
        String imageUri = "drawable://" + link;
        return imageUri;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }

    public static void SnackbarWin(View view, String tekstWiadomosci) {
        Snackbar snackbarAction = Snackbar.make(view, tekstWiadomosci, Snackbar.LENGTH_LONG)
                .setAction("", null);
        View sbView = snackbarAction.getView();
        TextView tv = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.BLACK);
        tv.setGravity(Gravity.CENTER_HORIZONTAL);
        sbView.setBackgroundColor(Color.rgb(0, 255, 0));
        snackbarAction.show();

    }

    public static void SnackbarLose(View view, String tekstWiadomosci) {
        Snackbar snackbarAction = Snackbar.make(view, tekstWiadomosci, Snackbar.LENGTH_LONG)
                .setAction("", null);
        View sbView = snackbarAction.getView();
        TextView tv = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.BLACK);
        tv.setGravity(Gravity.CENTER_HORIZONTAL);
        sbView.setBackgroundColor(Color.rgb(255, 0, 0));
        snackbarAction.show();

    }
}
