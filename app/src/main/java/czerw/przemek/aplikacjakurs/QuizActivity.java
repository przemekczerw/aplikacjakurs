package czerw.przemek.aplikacjakurs;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class QuizActivity extends AppCompatActivity {

    @BindView(R.id.listaPytan)
    ListView listaPytan;
    @BindView(R.id.tvQuestion)
    TextView tvQuestion;
    @BindView(R.id.QuizActivity)
    RelativeLayout QuizActivity;
    @BindView(R.id.points)
    TextView pointsQuiz;
    @BindView(R.id.timer)
    TextView timer;
    @BindView(R.id.progressBar3)
    ProgressBar progressBar3;
    int answer;
    public int pointsInGame;
    AlertDialog.Builder alertDialog;
    QuizLista lista = new QuizLista();
    List<String> answerList = new ArrayList<>();
    QuizPytania pytanieZObiektu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        progressBar3.setMax(30);
        listaPytan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                czyPoprawnaOdpowiedz(answer, position);

            }
        });
    }

    QuizAsyncTask quizAsyncTask;

    @OnClick(R.id.startQuiz)
    public void startQuiz() {
        listaPytan.setAdapter(wrzucPytanieNaEkran());
        utworzNowyAsyncTask();

    }

    public void zakonczGreIWyswietlWynik() {
        quizAsyncTask.cancel(true);
        alertDialog = new AlertDialog.Builder(QuizActivity.this);
        alertDialog.setTitle("Ukończyłeś QUIZ");
        alertDialog.setMessage("Zdobyłeś: " + pointsInGame + " punktów!");
        alertDialog.setCancelable(false);


        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tvQuestion.setText("");
                pointsQuiz.setText("");
                timer.setText("");
                progressBar3.setMax(0);
                listaPytan.setAdapter(null);
                pointsInGame = 0;
                dialog.cancel();
                lista = new QuizLista();
            }
        });

        AlertDialog alert = alertDialog.create();
        alert.show();
    }


    public void czyPoprawnaOdpowiedz(Integer answer, Integer position) {
        if (answer == position) {
            NaszeMetody.SnackbarWin(QuizActivity, "Poprawna odpowiedź");
            pointsInGame = pointsInGame + Integer.valueOf(pointsQuiz.getText().toString());
            lista.listaPytanQuiz.remove(pytanieZObiektu);
            quizAsyncTask.cancel(true);
            czyListaJestPusta();
        } else {
            NaszeMetody.SnackbarLose(QuizActivity, "Błędna odpowiedź");
            lista.listaPytanQuiz.remove(pytanieZObiektu);
            quizAsyncTask.cancel(true);
            czyListaJestPusta();

        }

//  NaszeMetody.ShowMessage(String.valueOf(pointsInGame));

    }

    public void czyListaJestPusta() {
        if (lista.listaPytanQuiz.size() != 0) {
            listaPytan.setAdapter(wrzucPytanieNaEkran());
            utworzNowyAsyncTask();
        } else {
            zakonczGreIWyswietlWynik();
        }
    }

    public void utworzNowyAsyncTask() {
        quizAsyncTask = new QuizAsyncTask(QuizActivity.this, pytanieZObiektu);
        quizAsyncTask.execute(30);
    }

    public Integer losujPytanie() {
//        int item;
//        Random r = new Random();
//        item = r.nextInt(lista.listaPytanQuiz.size());
        return (int) (Math.random() * (lista.listaPytanQuiz.size()));


    }

    public void usunPytaniePoCzasie() {
//      do uzupełnienia!


    }


    public ArrayAdapter wrzucPytanieNaEkran() {


        pytanieZObiektu = lista.listaPytanQuiz.get(losujPytanie());
        answerList.clear();
        answerList.add(pytanieZObiektu.getOdp1());
        answerList.add(pytanieZObiektu.getOdp2());
        answerList.add(pytanieZObiektu.getOdp3());
        answerList.add(pytanieZObiektu.getOdp4());

        answer = pytanieZObiektu.getAnswer();
        ArrayAdapter myAdapter = new ArrayAdapter(QuizActivity.this, android.R.layout.simple_list_item_1, answerList);

        tvQuestion.setText(pytanieZObiektu.getPytanie());

        return myAdapter;
    }


}
