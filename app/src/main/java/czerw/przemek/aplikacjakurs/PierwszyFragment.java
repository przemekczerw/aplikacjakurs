package czerw.przemek.aplikacjakurs;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class PierwszyFragment extends Fragment {


    public PierwszyFragment() {
        // Required empty public constructor
    }

    TextView tv;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_pierwszy, container, false);
        tv = (TextView) v.findViewById(R.id.tekscik);

        Button cofnijDoDrugiego = (Button) v.findViewById(R.id.backToDrugiFragment);

        cofnijDoDrugiego.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getFragmentManager().getBackStackEntryCount() != 0) {
                    getFragmentManager().popBackStack();
                }
            }
        });

        return v;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String msg = "";
        Bundle bunle = getArguments();
        if (bunle != null) {
            msg = bunle.getString("tajne");
        }
        if (!msg.isEmpty()) {
            tv.setText(msg);
        }
    }

}
