package czerw.przemek.aplikacjakurs;

import android.content.DialogInterface;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class GraActivity extends AppCompatActivity {
    private static final String SAVED_COMPUTER_SCORE = "SAVED_COMPUTER_SCORE";
    private static final String SAVED_USER_SCORE = "SAVED_USER_SCORE";

    @BindView(R.id.kamien)
    ImageButton kamien;
    @BindView(R.id.papier)
    ImageButton papier;
    @BindView(R.id.nozyce)
    ImageButton nozyce;
    @BindView(R.id.computerCounterPoints)
    TextView computerCounterPoints;
    @BindView(R.id.userCounterPoints)
    TextView userCounterPoints;
    @BindView(R.id.userPick)
    ImageView userPick;
    @BindView(R.id.activity_gra)
    LinearLayout activity_gra;
    @BindView(R.id.computerPick)
    ImageView computerPick;
    Uri SAVED_USER_PIC, SAVED_COMPUTER_PIC;
    int computerPoints = 0;
    int userPoints = 0;
    int roundCounter = 0;

    public static final int[] imageTable = new int[]{
            R.drawable.papier,
            R.drawable.kamien,
            R.drawable.nozyce
    };
    int PAPIER = 0;
    int KAMIEN = 1;
    int NOZYCE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gra);
        ButterKnife.bind(this);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SAVED_COMPUTER_SCORE, computerPoints);
        outState.putInt(SAVED_USER_SCORE, userPoints);


    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        computerPoints = savedInstanceState.getInt(SAVED_COMPUTER_SCORE);
        computerCounterPoints.setText(String.valueOf(computerPoints));
        userPoints = savedInstanceState.getInt(SAVED_USER_SCORE);
        userCounterPoints.setText(String.valueOf(userPoints));

    }

    public void SnackbarWin() {
        Snackbar snackbarAction = Snackbar.make(activity_gra, "You Win", Snackbar.LENGTH_LONG)
                .setAction("", null);
        View sbView = snackbarAction.getView();
        TextView tv = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.BLACK);
        sbView.setBackgroundColor(Color.rgb(0, 255, 0));
        snackbarAction.show();

    }

    public void SnackbarLose() {
        Snackbar snackbarAction = Snackbar.make(activity_gra, "You Lose", Snackbar.LENGTH_LONG)
                .setAction("", null);
        View sbView = snackbarAction.getView();
        TextView tv = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.BLACK);
        sbView.setBackgroundColor(Color.rgb(255, 0, 0));
        snackbarAction.show();

    }

    public void SnackbarDraw() {
        Snackbar snackbarAction = Snackbar.make(activity_gra, "Draw", Snackbar.LENGTH_LONG)
                .setAction("", null);
        View sbView = snackbarAction.getView();
        TextView tv = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.BLACK);
        sbView.setBackgroundColor(Color.rgb(255, 255, 0));
        snackbarAction.show();

    }


    @OnClick(R.id.kamien)
    public void kamienPicked() {

        userPick.setImageResource(R.drawable.kamien);
        int rnd = random();

        if (rnd == PAPIER) {
            computerPick.setImageResource(R.drawable.papier);
            computerPoints++;
            SnackbarLose();
            computerCounterPoints.setText(String.valueOf(computerPoints));
        } else if (rnd == KAMIEN) {
            SnackbarDraw();
            computerPick.setImageResource(R.drawable.kamien);
        } else {
            computerPick.setImageResource(R.drawable.nozyce);
            userPoints++;
            SnackbarWin();
            userCounterPoints.setText(String.valueOf(userPoints));
        }
        if ((userPoints == 5) || (computerPoints == 5)) {
            winLoseAlert();
        }
    }
// klikniecie myszka

    @OnClick(R.id.papier)
    public void papierPicked() {

        userPick.setImageResource(R.drawable.papier);
        int rnd = random();

        if (rnd == PAPIER) {
            computerPick.setImageResource(R.drawable.papier);
            SnackbarDraw();
        } else if (rnd == KAMIEN) {
            computerPick.setImageResource(R.drawable.kamien);
            userPoints++;
            SnackbarWin();
            userCounterPoints.setText(String.valueOf(userPoints));
        } else {
            computerPoints++;
            SnackbarLose();
            computerPick.setImageResource(R.drawable.nozyce);
            computerCounterPoints.setText(String.valueOf(computerPoints));
        }
        if ((userPoints == 5) || (computerPoints == 5)) {
            winLoseAlert();
        }
    }

    @OnClick(R.id.nozyce)
    public void nozycePicked() {
        userPick.setImageResource(R.drawable.nozyce);
        int rnd = random();

        if (rnd == PAPIER) {
            computerPick.setImageResource(R.drawable.papier);
            userPoints++;
            SnackbarWin();
            userCounterPoints.setText(String.valueOf(userPoints));
        } else if (rnd == KAMIEN) {
            computerPick.setImageResource(R.drawable.kamien);
            computerPoints++;
            SnackbarLose();
            computerCounterPoints.setText(String.valueOf(computerPoints));
        } else {
            computerPick.setImageResource(R.drawable.nozyce);
            SnackbarDraw();
        }
        if ((userPoints == 5) || (computerPoints == 5)) {
            winLoseAlert();
        }
    }

    public int random() {
        int random = new Random().nextInt(3);
        return random;
    }

    public void winLoseAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                GraActivity.this);
        if (userPoints == 5) {
            builder.setMessage("Wygrałeś!");
        } else if (computerPoints == 5) {
            builder.setMessage("Przegrałeś");
        }
        builder.setTitle("Masz wiadomość").setPositiveButton("RESTART", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                restartScores();
            }
        });
        builder.setCancelable(false).show();


    }

    public void restartScores() {
        userPoints = 0;
        computerPoints = 0;
        userCounterPoints.setText(String.valueOf(""));
        computerCounterPoints.setText(String.valueOf(""));
        computerPick.setImageResource(0);
        userPick.setImageResource(0);

    }
}
