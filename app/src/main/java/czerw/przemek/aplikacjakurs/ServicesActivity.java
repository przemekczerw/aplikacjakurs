package czerw.przemek.aplikacjakurs;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ServicesActivity extends AppCompatActivity {
    Intent intent, intent2;
    @BindView(R.id.StartCounting)
    Button StartCounting;
    @BindView(R.id.StopCounting)
    Button StopCounting;
    MediaPlayer music;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_services);
        ButterKnife.bind(this);
        music = MediaPlayer.create(this, R.raw.tetris_soundtrack);
    }

    @OnClick(R.id.StartCounting)
    public void wystartuj() {

        music.start();
        intent = new Intent(ServicesActivity.this, ToastServices.class);
        this.startService(intent);


    }

    @OnClick(R.id.StopCounting)
    public void stopuj() {
        if (music.isPlaying()) {
            music.pause();
        }
        intent = new Intent(ServicesActivity.this, ToastServices.class);
        this.stopService(intent);
    }


}
