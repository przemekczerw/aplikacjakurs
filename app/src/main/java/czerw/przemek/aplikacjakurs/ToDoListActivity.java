package czerw.przemek.aplikacjakurs;

/**
 * Created by przemyslawczerw on 14.07.2017.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import czerw.przemek.aplikacjakurs.Dane.ToDoItem;
import czerw.przemek.aplikacjakurs.Dane.ZapytaniaBazy;

public class ToDoListActivity extends AppCompatActivity {

    @OnClick(R.id.floatingActionButton)
    public void dodaj() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ToDoListActivity.this);
        alertDialog.setTitle("Wpisz zadanie do wykonania");

        final EditText editTextTytul = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        editTextTytul.setLayoutParams(lp);
        alertDialog.setView(editTextTytul);

        alertDialog.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(ToDoListActivity.this);
                alertDialog2.setTitle("Wpisz szczegóły zadania");

                final EditText editTextSzczegoly = new EditText(ToDoListActivity.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                editTextSzczegoly.setLayoutParams(lp);
                alertDialog2.setView(editTextSzczegoly);

                alertDialog2.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alertDialog2.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String zwrotIdWprowadzonego = polaczenieBazy.dodajToDoBazy(editTextSzczegoly.getText().toString(), 0);
                        listaZadan = polaczenieBazy.zwrocListeToDo();
                        pa = new ToDoAdapter(listaZadan, getApplicationContext());
                        mojRecycler.setAdapter(pa);


                    }
                });

                alertDialog2.show();


            }
        });

        alertDialog.show();


//        String zwrotIdWprowadzonego = polaczenieBazy.dodajToDoBazy("")
//        wyswietlacz.setText(zwrotIdWprowadzonego);
    }

    ZapytaniaBazy polaczenieBazy;
    List<ToDoItem> listaZadan;
    ToDoAdapter pa;
    RecyclerView mojRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_list);
        ButterKnife.bind(this);


        polaczenieBazy = new ZapytaniaBazy(ToDoListActivity.this);
        polaczenieBazy.otworzZapis();

        listaZadan = polaczenieBazy.zwrocListeToDo();

        mojRecycler = (RecyclerView) findViewById(R.id.mojRVi);

        // wybieramy manager layoutu (dostepny jeszcze Grid i StabbedGrid
        LinearLayoutManager llm = new LinearLayoutManager(this);
        // ustawiamy Manager Layoutu
        mojRecycler.setLayoutManager(llm);

        //utworzneie odstepu pomiedzy elementami listy
        DividerItemDecoration podzielenie = new DividerItemDecoration(mojRecycler.getContext(), llm.getOrientation());
        mojRecycler.addItemDecoration(podzielenie);

        // ustwaiamy nasz wlasny adapter do recyclerview
        pa = new ToDoAdapter(listaZadan, getApplicationContext());
        mojRecycler.setAdapter(pa);


    }
}
