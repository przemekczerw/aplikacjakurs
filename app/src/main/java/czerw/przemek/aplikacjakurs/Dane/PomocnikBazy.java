package czerw.przemek.aplikacjakurs.Dane;

/**
 * Created by przemyslawczerw on 14.07.2017.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


// sluzy lacznie z baza danych nasz pomocnik


public class PomocnikBazy extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "BazaFilmow.db";


    private static final String SQL_UTWORZ_TABELE = "CREATE TABLE " + FilmyContract.FilmTabela.NAZWA_TABELI +
            " (" + FilmyContract.FilmTabela._ID + " INTEGER PRIMARY KEY," +
            FilmyContract.FilmTabela.KOLUMNA_TYTUL + " TEXT," +
            FilmyContract.FilmTabela.KOLUMNA_BUDZET + " INTEGER," +
            FilmyContract.FilmTabela.KOLUMNA_ROK_POWSTANIA + " TEXT)";

    /**
     * To jest rowne zapytania: CREATE TABLE FilmModel (film_id integer primary key, tytul text, budzet integer, rokPowstania text);
     */

    private static final String SQL_UTWORZ_TABELE_TODO = "CREATE TABLE " + FilmyContract.ToDoTabela.NAZWA_TABELI +
            " (" + FilmyContract.ToDoTabela._ID + " INTEGER PRIMARY KEY," +
            FilmyContract.ToDoTabela.KOLUMNA_NAZWA_ZADANIA + " TEXT," +
            FilmyContract.ToDoTabela.KOLUMNA_NAZWA_SZCZEGOLY + " TEXT," +
            FilmyContract.ToDoTabela.KOLUMNA_CZY_ZROBIONE + " INTEGER)";


    public static final String SQL_SKASUJ_TABELE = "DROP TABLE IF EXISTS " + FilmyContract.FilmTabela.NAZWA_TABELI;
    public static final String SQL_SKASUJ_TABELE_TODO = "DROP TABLE IF EXISTS " + FilmyContract.ToDoTabela.NAZWA_TABELI;


    public PomocnikBazy(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_UTWORZ_TABELE);
        db.execSQL(SQL_UTWORZ_TABELE_TODO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_SKASUJ_TABELE);
        db.execSQL(SQL_SKASUJ_TABELE_TODO);
        onCreate(db);
    }
}