package czerw.przemek.aplikacjakurs.Dane;

/**
 * Created by przemyslawczerw on 14.07.2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;


public class ZapytaniaBazy {

    private SQLiteDatabase baza;
    private PomocnikBazy pomocnikBazy;

    public ZapytaniaBazy(Context context) {
        pomocnikBazy = new PomocnikBazy(context);
    }

    public void otworzZapis() throws SQLException {
        baza = pomocnikBazy.getWritableDatabase();
    }

    public void otworzOdczyt() {
        baza = pomocnikBazy.getReadableDatabase();
    }

    public void zamknij() {
        baza.close();
    }

    public String dodajDoBazy(String nazwaFilmu, int budzet, String rokPowstania) {
        ContentValues wartosci = new ContentValues();
        wartosci.put(FilmyContract.FilmTabela.KOLUMNA_TYTUL, nazwaFilmu);
        wartosci.put(FilmyContract.FilmTabela.KOLUMNA_BUDZET, budzet);
        wartosci.put(FilmyContract.FilmTabela.KOLUMNA_ROK_POWSTANIA, rokPowstania);

        long numerWiersza = baza.insert(FilmyContract.FilmTabela.NAZWA_TABELI, null, wartosci);

        return String.valueOf(numerWiersza);
    }


    public List<FilmModel> zwrocListeFilmow() {
        ArrayList<FilmModel> listaFilmow = new ArrayList<FilmModel>();

        Cursor cursor = baza.query(FilmyContract.FilmTabela.NAZWA_TABELI, null, null, null, null, null, null);


        FilmModel filmModel;

        // sprawdzamy czy jest cos na liscie
        if (cursor.getCount() > 0) {
            // kursor przechodzi po kazdym elemncie listy
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToNext();
                filmModel = new FilmModel();
                filmModel.setId(cursor.getInt(0));
                filmModel.setTytul(cursor.getString(1));
                filmModel.setBudzet(cursor.getInt(2));
                filmModel.setRokPowstania(cursor.getString(3));
                listaFilmow.add(filmModel);

            }
        }

        return listaFilmow;
    }


    public void usunWierszBaza(String id) {
        String selection = FilmyContract.FilmTabela._ID + " = ?";
        String selectionArgumenty[] = {id};
        // powyzsze rowna sie temu: np.  WHERE id = ? zamienia na WHERE id = 1;
        baza.delete(FilmyContract.FilmTabela.NAZWA_TABELI, selection, selectionArgumenty);

    }

    public void edytujWiersz(String id, int budzet) {
        // ustalamy jaka kolumne chcemy zmienic i podajemy wartosc na jaka zmieniamy
        ContentValues wartosci = new ContentValues();
        wartosci.put(FilmyContract.FilmTabela.KOLUMNA_BUDZET, budzet);


        String selection = FilmyContract.FilmTabela._ID + " = ?";
        String selectionArgumenty[] = {id};

        baza.update(FilmyContract.FilmTabela.NAZWA_TABELI, wartosci, selection, selectionArgumenty);

    }

    public void czyscielBazy() {
        baza.execSQL("DELETE FROM " + FilmyContract.FilmTabela.NAZWA_TABELI);

        // korzystamy z  baza.rawQuery(); wtedy kiedy chcemu uzyc SELECT bo on zwraca Cursor
    }


    public List<ToDoItem> zwrocListeToDo() {
        ArrayList<ToDoItem> listaZrobic = new ArrayList<ToDoItem>();

        Cursor cursor = baza.query(FilmyContract.ToDoTabela.NAZWA_TABELI, null, null, null, null, null, null);


        ToDoItem toDoModel;

        // sprawdzamy czy jest cos na liscie
        if (cursor.getCount() > 0) {
            // kursor przechodzi po kazdym elemncie listy
            for (int i = 0; i < cursor.getCount(); i++) {
                cursor.moveToNext();
                toDoModel = new ToDoItem();
                toDoModel.setId(cursor.getInt(0));
                toDoModel.setNazwaZadania(cursor.getString(1));
                toDoModel.setZrobione(cursor.getInt(2));
                listaZrobic.add(toDoModel);

            }
        }

        return listaZrobic;
    }


    public String dodajToDoBazy(String nazwaZadania, int zrobione) {
        ContentValues wartosci = new ContentValues();
        wartosci.put(FilmyContract.ToDoTabela.KOLUMNA_NAZWA_ZADANIA, nazwaZadania);
        wartosci.put(FilmyContract.ToDoTabela.KOLUMNA_CZY_ZROBIONE, zrobione);

        long numerWiersza = baza.insert(FilmyContract.ToDoTabela.NAZWA_TABELI, null, wartosci);

        return String.valueOf(numerWiersza);
    }

    public void usunWierszToDo(String id) {
        String selection = FilmyContract.ToDoTabela._ID + " = ?";
        String selectionArgumenty[] = {id};
        // powyzsze rowna sie temu: np.  WHERE id = ? zamienia na WHERE id = 1;
        baza.delete(FilmyContract.ToDoTabela.NAZWA_TABELI, selection, selectionArgumenty);

    }

    public void edytujWierszToDo(String nazwa, String id) {
        // ustalamy jaka kolumne chcemy zmienic i podajemy wartosc na jaka zmieniamy
        ContentValues wartosci = new ContentValues();
        wartosci.put(FilmyContract.ToDoTabela.KOLUMNA_NAZWA_ZADANIA, nazwa);


        String selection = FilmyContract.ToDoTabela._ID + " = ?";
        String selectionArgumenty[] = {id};

        baza.update(FilmyContract.ToDoTabela.NAZWA_TABELI, wartosci, selection, selectionArgumenty);

    }

}

