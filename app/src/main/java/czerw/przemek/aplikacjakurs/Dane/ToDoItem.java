package czerw.przemek.aplikacjakurs.Dane;

/**
 * Created by przemyslawczerw on 14.07.2017.
 */

public class ToDoItem {
    private int id;
    private String nazwaZadania;
    private String szczegoly;

    public String getSzczegoly() {
        return szczegoly;
    }

    public void setSzczegoly(String szczegoly) {
        this.szczegoly = szczegoly;
    }

    private int zrobione;

    public ToDoItem(int id, String nazwaZadania, int zrobione) {
        this.id = id;
        this.nazwaZadania = nazwaZadania;
        this.zrobione = zrobione;
    }

    public ToDoItem() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNazwaZadania() {
        return nazwaZadania;
    }

    public void setNazwaZadania(String nazwaZadania) {
        this.nazwaZadania = nazwaZadania;
    }

    public int getZrobione() {
        return zrobione;
    }

    public void setZrobione(int zrobione) {
        this.zrobione = zrobione;
    }
}