package czerw.przemek.aplikacjakurs.Dane;

/**
 * Created by przemyslawczerw on 14.07.2017.
 */

public class FilmModel {
    public String getTytul() {
        return tytul;
    }

    public void setTytul(String tytul) {
        this.tytul = tytul;
    }

    public int getBudzet() {
        return budzet;
    }

    public void setBudzet(int budzet) {
        this.budzet = budzet;
    }

    public String getRokPowstania() {
        return rokPowstania;
    }

    public void setRokPowstania(String rokPowstania) {
        this.rokPowstania = rokPowstania;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    // alt + insert -> windows   cmd + n -> mac
    private int id;
    private String tytul;
    private int budzet;
    private String rokPowstania;

    public String zwrocWiersz() {
        return " " + id + "Film: " + tytul + " o budżecie " + budzet + " z roku " + rokPowstania;
    }

}
