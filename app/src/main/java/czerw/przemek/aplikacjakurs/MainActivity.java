package czerw.przemek.aplikacjakurs;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import butterknife.ButterKnife;
import butterknife.OnClick;
import czerw.przemek.aplikacjakurs.db.LekcjaFragmentyActivity;

public class MainActivity extends AppCompatActivity {
    final String TAG = "PrzemekApp";
    String settedText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        NaszeMetody.InicjalizacjaNaszeMetody(getApplicationContext());




    }
    @OnClick(R.id.buttonGoToGraActivity)
    public void goToGraActivity() {
        Intent intent = new Intent(this, GraActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.buttonGoToRegisterActivity)
    public void goToRegisterActivity() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.buttonGoToServicesActivity)
    public void goToServicesActivity() {
        Intent intent = new Intent(this, ServicesActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.buttonGoToImageLoaderActivity)
    public void goToImageLoaderActivity() {
        Intent intent = new Intent(this, ImageLoaderActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.buttonGoCellPhoneActivity)
    public void goToCellPhoneActivity() {
        Intent intent = new Intent(this, CellPhoneActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.buttonGoToRestauracjaActivity)
    public void goToRestauracjaActivity() {
        Intent intent = new Intent(this, RestauracjaActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.buttonGoToSmsActivity)
    public void goTobuttonGoToSmsActivity() {
        Intent intent = new Intent(this, SMSActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.buttonGoToFragmenty)
    public void goToButtonGoToFragmenty() {
        Intent intent = new Intent(this, LekcjaFragmentyActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.buttonGoToDoList)
    public void goTobuttonGoToDoList() {
        Intent intent = new Intent(this, ToDoListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.buttonGoToTakeAPicture)
    public void goTobuttonGoToTakeAPicture() {
        Intent intent = new Intent(this, TakeAPhotoActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.buttonGoToAsyncTask)
    public void goTobuttonGoToAsyncTask() {
        Intent intent = new Intent(this, AsyncTaskActivity.class);
        startActivity(intent);
    }



    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "MainActivity: onRestart()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "MainActivity: onStart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "MainActivity: onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "MainActivity: onPause()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "MainActivity: onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "MainActivity: onDestroy()");
    }

}
