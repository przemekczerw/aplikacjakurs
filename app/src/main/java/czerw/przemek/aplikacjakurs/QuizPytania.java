package czerw.przemek.aplikacjakurs;

/**
 * Created by przemyslawczerw on 27.07.2017.
 */

public class QuizPytania {


    Integer answer;
    Boolean czyPytanieByloZadane;
    String pytanie;
    String odp1, odp2, odp3, odp4;

    public QuizPytania(Integer answer, Boolean czyPytanieByloZadane, String pytanie, String odp1, String odp2, String odp3, String odp4) {
        this.answer = answer;
        this.czyPytanieByloZadane = czyPytanieByloZadane;
        this.pytanie = pytanie;
        this.odp1 = odp1;
        this.odp2 = odp2;
        this.odp3 = odp3;
        this.odp4 = odp4;
    }

    public Integer getAnswer() {
        return answer;
    }

    public void setAnswer(Integer answer) {
        this.answer = answer;
    }

    public Boolean getCzyPytanieByloZadane() {
        return czyPytanieByloZadane;
    }

    public void setCzyPytanieByloZadane(Boolean czyPytanieByloZadane) {
        this.czyPytanieByloZadane = czyPytanieByloZadane;
    }

    public String getPytanie() {
        return pytanie;
    }

    public void setPytanie(String pytanie) {
        this.pytanie = pytanie;
    }

    public String getOdp1() {
        return odp1;
    }

    public void setOdp1(String odp1) {
        this.odp1 = odp1;
    }

    public String getOdp2() {
        return odp2;
    }

    public void setOdp2(String odp2) {
        this.odp2 = odp2;
    }

    public String getOdp3() {
        return odp3;
    }

    public void setOdp3(String odp3) {
        this.odp3 = odp3;
    }

    public String getOdp4() {
        return odp4;
    }

    public void setOdp4(String odp4) {
        this.odp4 = odp4;
    }
}
