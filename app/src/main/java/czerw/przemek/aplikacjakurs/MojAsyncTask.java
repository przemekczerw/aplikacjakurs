package czerw.przemek.aplikacjakurs;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;

/**
 * Created by przemyslawczerw on 25.07.2017.
 */

public class MojAsyncTask extends AsyncTask<Integer, Integer, String> {

    TextView textView;
    ProgressDialog progress;
    Context context;

    public MojAsyncTask(TextView textView, Context context) {
        this.textView = textView;
        this.context = context;
    }

    @Override
    protected String doInBackground(Integer... params) {

        int tmp = params[0];
        for (int i = tmp; i > -1; i--) {
            publishProgress(i);
            NaszeMetody.ThreadSleep(1000);
        }


        return "Zakończono odliczanie";
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        int counter = values[0];

        textView.setText(String.valueOf(counter));
        progress.setProgress(counter);
    }

    @Override
    protected void onPostExecute(String result) {
        NaszeMetody.ShowMessage(result);
        progress.dismiss();


//        textView.setText("Cześć " + result);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        progress = new ProgressDialog(context);
        progress.setProgressStyle(progress.STYLE_HORIZONTAL);
        progress.setMax(10);
        progress.setTitle("Odliczam czas");
        progress.setMessage("idzie to");
        progress.show();
//        textView.setText("elo");
    }
}
