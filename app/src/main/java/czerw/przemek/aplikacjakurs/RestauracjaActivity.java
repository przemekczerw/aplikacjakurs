package czerw.przemek.aplikacjakurs;

/**
 * Created by przemyslawczerw on 14.07.2017.
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;

import org.greenrobot.greendao.database.Database;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import czerw.przemek.aplikacjakurs.db.DaoMaster;
import czerw.przemek.aplikacjakurs.db.DaoSession;
import czerw.przemek.aplikacjakurs.db.Koszyk;
import czerw.przemek.aplikacjakurs.db.KoszykDao;
import czerw.przemek.aplikacjakurs.db.Produkt;
import czerw.przemek.aplikacjakurs.db.ProduktDao;

public class RestauracjaActivity extends AppCompatActivity {

    @OnClick(R.id.floatingActionButton)
    public void dodaj() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(RestauracjaActivity.this);
        alertDialog.setTitle("Wpisz produkt");

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        LinearLayout.LayoutParams doTextu = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        LinearLayout v = new LinearLayout(this);
        v.setOrientation(LinearLayout.VERTICAL);
        v.setLayoutParams(lp);

        final EditText nowyText = new EditText(this);
        nowyText.setHint("Wpisz nazwę produktu");
        nowyText.setLayoutParams(doTextu);
        v.addView(nowyText);

        final EditText textCena = new EditText(this);
        textCena.setHint("Wpisz cenę");
        textCena.setLayoutParams(doTextu);
        v.addView(textCena);


        alertDialog.setView(v);

        alertDialog.setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                // tworzymy nowy produkt
                Produkt pr = new Produkt();
                pr.setNazwa(nowyText.getText().toString());
                pr.setCena(Float.valueOf(textCena.getText().toString()));
                // wstawiamy produkt do bazy
                produktDao.insert(pr);

                odczytajDaneBaza();

                pa = new RestauracjaAdapter(listaProduktow, getApplicationContext());
                mojRecycler.setAdapter(pa);
            }
        });

        alertDialog.show();

    }

    RestauracjaAdapter pa;
    List<Produkt> listaProduktow;
    RecyclerView mojRecycler;

    private DaoSession daoSession;
    ProduktDao produktDao;
    KoszykDao koszykDao;

    public void odczytajDaneBaza() {
        // odczytujemy liste danych z bazy
        listaProduktow = produktDao.queryBuilder().list();
        //  listaProduktow = daoSession.getProduktDao().loadAll();
    }

    Toolbar toolbar;
    Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restauracja);

        ButterKnife.bind(this);


        listaProduktow = new ArrayList<>();

        // otwieramy polaczenie z baza
        DaoMaster.DevOpenHelper pomocnik = new DaoMaster.DevOpenHelper(this, "users-db");
        db = pomocnik.getWritableDb();
        // tworzymy nowa sesje
        daoSession = new DaoMaster(db).newSession();
        // laczymy sie z produktDao
        produktDao = daoSession.getProduktDao();
        koszykDao = daoSession.getKoszykDao();


        odczytajDaneBaza();


        mojRecycler = (RecyclerView) findViewById(R.id.mojRVi);

        // wybieramy manager layoutu (dostepny jeszcze Grid i StabbedGrid
        LinearLayoutManager llm = new LinearLayoutManager(this);
        // ustawiamy Manager Layoutu
        mojRecycler.setLayoutManager(llm);

        //utworzneie odstepu pomiedzy elementami listy
        DividerItemDecoration podzielenie = new DividerItemDecoration(mojRecycler.getContext(), llm.getOrientation());
        mojRecycler.addItemDecoration(podzielenie);

        // ustwaiamy nasz wlasny adapter do recyclerview
        pa = new RestauracjaAdapter(listaProduktow, RestauracjaActivity.this);
        mojRecycler.setAdapter(pa);

        toolbar = (Toolbar) findViewById(R.id.mojToolbar);
        toolbar.inflateMenu(R.menu.menu_koszyk);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.idRestaurant) {
                    Intent i = new Intent(getApplicationContext(), KoszykActivity.class);
                    startActivity(i);
                }
                return false;
            }
        });

    }

    public void pokazSume() {
        DaoMaster.DevOpenHelper pomocnik = new DaoMaster.DevOpenHelper(this, "users-db");
        db = pomocnik.getWritableDb();
        // tworzymy nowa sesje
        daoSession = new DaoMaster(db).newSession();
        // laczymy sie z produktDao
        produktDao = daoSession.getProduktDao();
        koszykDao = daoSession.getKoszykDao();

        // pobieram  liste elementow z koszyka
        List<Koszyk> koszyk = koszykDao.queryBuilder().list();
        float suma = 0;
        // na kazdym elemencie koszyka robie obliczenia
        for (Koszyk item : koszyk) {
            // pobieram sobie ilosc danego produktu w koszyku
            int ilosc = item.getIlosc();
            // sprawdzam cene tego produktu
            List<Produkt> listaProdukt = produktDao.queryBuilder().where(ProduktDao.Properties.Id.eq(item.getProduktId())).list();
            float cena = listaProdukt.get(0).getCena();
            // mnoze ilosc * cena  i dodaje do sumy koszyka
            suma = suma + ((float) ilosc * cena);
        }

        // uakatualnia toolbara o wartosc tej sumy
        toolbar.setTitle(String.valueOf(suma) + " zł");

    }
}
