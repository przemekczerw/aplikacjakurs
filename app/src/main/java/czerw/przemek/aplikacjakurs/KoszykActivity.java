package czerw.przemek.aplikacjakurs;

/**
 * Created by przemyslawczerw on 14.07.2017.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.greenrobot.greendao.database.Database;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import czerw.przemek.aplikacjakurs.db.DaoMaster;
import czerw.przemek.aplikacjakurs.db.DaoSession;
import czerw.przemek.aplikacjakurs.db.Koszyk;
import czerw.przemek.aplikacjakurs.db.KoszykDao;
import czerw.przemek.aplikacjakurs.db.Produkt;
import czerw.przemek.aplikacjakurs.db.ProduktDao;

public class KoszykActivity extends AppCompatActivity {

    KoszykAdapter pa;
    List<Koszyk> listKoszyk;
    RecyclerView mojRecycler;
    Button wyslijZakupy;

    private DaoSession daoSession;
    KoszykDao koszykDao;

    String wyslijSuma;

    public void odczytajDaneBaza() {
        // odczytujemy liste danych z bazy
        listKoszyk = koszykDao.queryBuilder().list();
    }

    Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_koszyk);
        ButterKnife.bind(this);


        listKoszyk = new ArrayList<>();

        // otwieramy polaczenie z baza
        DaoMaster.DevOpenHelper pomocnik = new DaoMaster.DevOpenHelper(this, "users-db");
        db = pomocnik.getWritableDb();
        // tworzymy nowa sesje
        daoSession = new DaoMaster(db).newSession();
        // laczymy sie z produktDao
        koszykDao = daoSession.getKoszykDao();

        odczytajDaneBaza();

        mojRecycler = (RecyclerView) findViewById(R.id.mojRVi);

        // wybieramy manager layoutu (dostepny jeszcze Grid i StabbedGrid
        LinearLayoutManager llm = new LinearLayoutManager(this);
        // ustawiamy Manager Layoutu
        mojRecycler.setLayoutManager(llm);

        //utworzneie odstepu pomiedzy elementami listy
        DividerItemDecoration podzielenie = new DividerItemDecoration(mojRecycler.getContext(), llm.getOrientation());
        mojRecycler.addItemDecoration(podzielenie);

        // ustwaiamy nasz wlasny adapter do recyclerview
        pa = new KoszykAdapter(listKoszyk, KoszykActivity.this);
        mojRecycler.setAdapter(pa);

        pokazSume();

        wyslijZakupy = (Button) findViewById(R.id.button_wyslijZakupy);

        wyslijZakupy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otworzActivityPoWynik();
            }
        });


    }

    static final int WYSLIJ_SMS = 1;

    public void otworzActivityPoWynik() {
        Intent intent = new Intent(this, SMSActivity.class);
        intent.putExtra("suma", wyslijSuma);
        startActivityForResult(intent, WYSLIJ_SMS);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == WYSLIJ_SMS) {
            if (resultCode == RESULT_OK) {
                NaszeMetody.ShowMessage("Wyslano SMS");
            } else if (resultCode == RESULT_CANCELED) {
                NaszeMetody.ShowMessage("Nie wysłano SMS");
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setResult(RESULT_CANCELED);
    }

    @BindView(R.id.mojTextView)
    TextView sumaText;

    public void pokazSume() {
        DaoMaster.DevOpenHelper pomocnik = new DaoMaster.DevOpenHelper(this, "users-db");
        db = pomocnik.getWritableDb();
        // tworzymy nowa sesje
        daoSession = new DaoMaster(db).newSession();
        // laczymy sie z produktDao
        ProduktDao produktDao = daoSession.getProduktDao();
        koszykDao = daoSession.getKoszykDao();

        // pobieram  liste elementow z koszyka
        List<Koszyk> koszyk = koszykDao.queryBuilder().list();
        float suma = 0;
        // na kazdym elemencie koszyka robie obliczenia
        for (Koszyk item : koszyk) {
            // pobieram sobie ilosc danego produktu w koszyku
            int ilosc = item.getIlosc();
            // sprawdzam cene tego produktu
            List<Produkt> listaProdukt = produktDao.queryBuilder().where(ProduktDao.Properties.Id.eq(item.getProduktId())).list();
            float cena = listaProdukt.get(0).getCena();
            // mnoze ilosc * cena  i dodaje do sumy koszyka
            suma = suma + ((float) ilosc * cena);
            wyslijSuma = String.valueOf(suma);
        }

        // uakatualnia toolbara o wartosc tej sumy
        sumaText.setText(String.valueOf(suma) + " zł");
    }
}