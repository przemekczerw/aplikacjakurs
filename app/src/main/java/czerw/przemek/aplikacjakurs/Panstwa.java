package czerw.przemek.aplikacjakurs;

/**
 * Created by przemyslawczerw on 14.06.2017.
 */

public class Panstwa {

    String nazwa_panstwa;
    String stolica_panstwa;
    int flaga;

    public Panstwa(String nazwa_panstwa, String stolica_panstwa, int flaga) {
        this.nazwa_panstwa = nazwa_panstwa;
        this.stolica_panstwa = stolica_panstwa;
        this.flaga = flaga;
    }

    public String getNazwa_panstwa() {
        return nazwa_panstwa;
    }

    public void setNazwa_panstwa(String nazwa_panstwa) {
        this.nazwa_panstwa = nazwa_panstwa;
    }

    public String getStolica_panstwa() {
        return stolica_panstwa;
    }

    public void setStolica_panstwa(String stolica_panstwa) {
        this.stolica_panstwa = stolica_panstwa;
    }

    public int getFlaga() {
        return flaga;
    }

    public void setFlaga(int flaga) {
        this.flaga = flaga;
    }
}
