package czerw.przemek.aplikacjakurs;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ZdjecieFragment extends Fragment {


    public ZdjecieFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_zdjecie, container, false);
        ImageView imageView = (ImageView) v.findViewById(R.id.iv_fragment_zdjecie);
        imageView.setImageResource(R.drawable.code_more);
        return v;
    }

}
