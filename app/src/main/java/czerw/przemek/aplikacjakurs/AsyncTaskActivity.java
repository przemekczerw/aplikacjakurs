package czerw.przemek.aplikacjakurs;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AsyncTaskActivity extends AppCompatActivity {

    @BindView(R.id.textView2)
    TextView textView2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_async_task);

        ButterKnife.bind(this);
    }


    @OnClick(R.id.button)
    public void wykonajAsyncTask() {
        new MojAsyncTask(textView2, AsyncTaskActivity.this).execute(10);
    }

    @OnClick(R.id.buttonPeddingIntent)
    public void wykonajPendingIntent() {
        Intent intent = new Intent(this, QuizActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        try {

            pendingIntent.send();

        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.buttonSetAlarm)
    public void ustawAlarm() {
        Intent intent = new Intent(this, MojReceiver.class);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 1, intent, 0);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 3000, pendingIntent);

    }

    Handler handler;

    @OnClick(R.id.buttonHandler)
    public void testujHandler() {

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                textView2.setText(msg.obj.toString());
            }
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                //1. Wylosuj
                Random random = new Random();
                int wylosowanaLiczba = random.nextInt(100);
                // 2. uspij
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Message wiadomosc = new Message();
                wiadomosc.obj = wylosowanaLiczba;
                // wyslij
                handler.sendMessage(wiadomosc);
            }
        }).start();
    }
}
